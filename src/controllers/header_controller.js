import { Controller } from "stimulus"

export default class extends Controller {

  static targets = ["name", "summaryName"]
  
  initialize() {
    // this is the value before the user clicks on the input
    this.namePersistent = this.nameTarget.innerText;
  }
  
  endEditMode() {
    this.nameTarget.setAttribute('contenteditable', 'false');
    this.nameTarget.setAttribute('contenteditable', 'true');
  }
  
  startType(event) {
    const code = event.keyCode;
    const beforeTypeValue = this.nameTarget.innerText;
    if (code === 13) { // enter
      this.endEditMode();
      event.preventDefault();
      
      // we don't wait for the server's response but we could
      sendToBackend(beforeTypeValue); 
      
      this.namePersistent = this.nameTarget.innerText;
      this.summaryNameTargets.forEach(t => { t.innerText = beforeTypeValue; });
      return;
    }
    if (code === 27) { // escape
      this.endEditMode();
      // reset the value to the one before the editing mode started
      this.nameTarget.innerText = this.namePersistent;
      return;
    }
    if (beforeTypeValue.length >= 25 && (code >= 48 || code === 32)) {
      // if text too long and (character is graphic or is space)
      event.preventDefault();
    }
  }
  
}


function sendToBackend(value) {
  // AJAX
}

